# course-jenkins

**Line Notif**

ใช้สำหรับ แจ้งเตือนเข้ากลุ่ม Line เมื่อ Job ทำงานเสร็จ
จำเป็นต้อง invite Line Notify เข้ามาในกลุ่ม และลงทะเบียน Service เพื่อขอ Token มาใช้งานด้วยเว็บ https://notify-bot.line.me/en/

แจ้งเตือนด้วยคำสั่ง
```sh
curl https://notify-api.line.me/api/notify -H 'Authorization: Bearer TOKEN' -d 'message=hello world‘
```

**Lab**

1. Invite Line Notify เข้ามาในกลุ่ม line
2. ลงทะเบียน Service เพื่อขอ Token มาใช้งาน
3. นำ Token มาใช้งานผ่าน pipeline

**ref**
- [https://notify-bot.line.me/en/](https://notify-bot.line.me/en/)
- [https://medium.com/@nattaponsirikamonnet/มาลอง-line-notify-กันเถอะ-พื้นฐาน-65a7fc83d97f](https://medium.com/@nattaponsirikamonnet/มาลอง-line-notify-กันเถอะ-พื้นฐาน-65a7fc83d97f)